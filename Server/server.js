//require express
var express = require('express');
var app = express();

//require coupon module
var coupon = require('./generateCoupon');

var couponCode = coupon.code();
couponCode = couponCode.toString();

//define routes
app.get('/', function(req, res){
	res.send(couponCode); 
});

app.listen(3001);